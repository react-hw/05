import React from "react";
import { Formik, Form } from "formik";

// import "./OrderPage_1.scss";

import Input from "../components/Form/components/Input.jsx";

const OrderPage = () => {
  const initialValues = {
    firstname: "",
    lastname: "",
    age: "",
    country: "",
    company: "",
    address: "",
    apartment: "",
    city: "",
    state: "",
    postcode: "",
    phone: "",
  };
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => console.log(values)}
    >
      {({ errors, touched }) => (
        <Form className="form">
          <fieldset className="fieldset-outer">
            <Input
              label={"First Name*"}
              placeholder={"First Name"}
              name={"firstname"}
              error={errors.firstname && touched.firstname}
            />
            <Input
              label={"Last Name*"}
              placeholder={"Last Name"}
              name={"lastname"}
              error={errors.lastname && touched.lastname}
            />
          </fieldset>
          <fieldset className="fieldset-outer">
            <Input
              label={"Age*"}
              placeholder={"Your full age"}
              name={"age"}
              error={errors.age && touched.age}
            />
            <Input
              label={"Company Name"}
              placeholder={"Company Name"}
              name={"company"}
              error={errors.company && touched.company}
            />
          </fieldset>
          <fieldset className="fieldset-outer">
            <Input
              label={"Country / Region*"}
              placeholder={"Country / Region"}
              name={"country"}
              error={errors.country && touched.country}
            />
            <Input
              label={"State*"}
              placeholder={"State"}
              name={"state"}
              error={errors.state && touched.state}
            />
            <Input
              label={"City*"}
              placeholder={"Town / City"}
              name={"city"}
              error={errors.city && touched.city}
            />
          </fieldset>
          <fieldset className="fieldset-outer">
            <Input
              label={"Street Address*"}
              placeholder={"Street Address"}
              name={"address"}
              error={errors.address && touched.address}
            />
            <Input
              label={"Apt, suite, unit"}
              placeholder={"Apt, suite, unit"}
              name={"apartment"}
              error={errors.apartment && touched.apartment}
            />

            <Input
              label={"Postal Code*"}
              placeholder={"Postal Code"}
              name={"postcode"}
              error={errors.postcode && touched.postcode}
            />
          </fieldset>
          <fieldset className="fieldset-outer">
            <Input
              className={"phone"}
              label={"Phone*"}
              placeholder={"Phone"}
              name={"phone"}
              error={errors.phone && touched.phone}
            />
          </fieldset>
        </Form>
      )}
    </Formik>
  );
};

export default OrderPage;
