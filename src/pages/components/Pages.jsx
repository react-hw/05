import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Pages.scss";

const Pages = ({ className, children }) => {
  return (
    <div className={cn("pages", className)}>{children}</div>
  );
};

Pages.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Pages;
