import React from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import Pages, { CardsWrap } from "./components";
import { CardToCart } from "../components/Cards";
import ButtonClassic from "../components/Buttons";
import { ModalFormToOrder } from "../components/Modals";

import { toggelModal } from "../store/modal_slices";
import { selectorIsModalForm, selectorCarts } from "../store/selectors";



const CartPage = (props) => {
  const { handleModalDelCart, onCurrentProduct, handleFavorite } = props;

  const dispatch = useDispatch();
  const isModalForm = useSelector(selectorIsModalForm);
  const carts = useSelector(selectorCarts);

  const handleModalForm = () => {
    dispatch(toggelModal({ modalName: "isModalForm" }));
  };

  return (
    <Pages>
      <>
        <ModalFormToOrder isOpen={isModalForm} onClose={handleModalForm} />
        <div className="pages__cart wrapper">
          <ButtonClassic className={"btn__to_cart"} onClick={handleModalForm}>
            TO ORDER
          </ButtonClassic>

          <CardsWrap>
            {carts.map((product) => {
              return (
                <CardToCart
                  key={product.vendorCode}
                  product={product}
                  onModalDelCart={() => {
                    handleModalDelCart();
                    onCurrentProduct(product);
                  }}
                  handleFavorite={handleFavorite}
                />
              );
            })}
          </CardsWrap>
        </div>
      </>
    </Pages>
  );
};

CartPage.propTypes = {
  carts: PropTypes.array,
  onCurrentProduct: PropTypes.func,
  handleModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CartPage;
