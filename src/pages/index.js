import HomePage from "./HomePage.jsx";
import CartPage from "./CartPage.jsx";
// import OrderPage from "./OrderPage_3.jsx";
import FavoritePage from "./FavoritePage.jsx";
import NotFoundPage from "./NotFoundPage.jsx";

export {
  HomePage,
  CartPage,
  // OrderPage,
  FavoritePage,
  NotFoundPage,
};
