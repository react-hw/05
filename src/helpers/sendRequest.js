const sendRequest = async (url) => {
  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error(`Request failed ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error("Request error", error);
    throw error;
  }
};

export default sendRequest;
