import React, { useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

import { ButtonCornerDelete, ButtonCornerHeart } from "../Buttons";
import CardContent from ".";

const CardToCart = (props) => {
  const { className, product, onModalDelCart, handleFavorite } = props;
  const [isFavorite, setIsFavorite] = useState(product.addedToFavorite);

  const toggleFavorite = () => {
    handleFavorite(product);
    setIsFavorite(!isFavorite);
  };

  return (
    <div className={cn("card", className)}>
      <ButtonCornerHeart
        className="btn__corner-left"
        onClick={toggleFavorite}
        isActive={isFavorite}
      />
      <ButtonCornerDelete onClick={onModalDelCart} />

      <CardContent product={product} />
    </div>
  );
};

CardToCart.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  handleModalDelCart: PropTypes.func,
  onModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CardToCart;
