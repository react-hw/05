import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

import { ButtonCornerCart, ButtonCornerDelete } from "../Buttons";
import CardContent from ".";

const CardFavorite = (props) => {
  const { className, product, onModalProduct, onModalDelFavorite } = props;
  const counterBasket = product.basketCounter;
  
  return (
    <div className={cn("card", className)}>
      <ButtonCornerCart
        onClick={onModalProduct}
        counterBasket={counterBasket}
        isActive={product.addedToCart}
      />
      <ButtonCornerDelete onClick={onModalDelFavorite} />

      <CardContent product={product} />
    </div>
  );
};

CardFavorite.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  onModalProduct: PropTypes.func,
  onModalDelFavorite: PropTypes.func,
};

export default CardFavorite;
