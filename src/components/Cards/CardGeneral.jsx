import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

import { ButtonCornerCart, ButtonCornerHeart } from "../Buttons";
import CardContent from "./";

const CardGeneral = (props) => {
  const { className, product, onModalProduct, handleFavorite } = props;
  const [isFavorite, setIsFavorite] = useState(product.addedToFavorite);
  const [counterBasket, setCounterBasket] = useState(product.basketCounter);

  useEffect(() => {
    setIsFavorite(product.addedToFavorite);
    setCounterBasket(product.basketCounter);
  }, [product]);

  const toggleFavorite = () => {
    handleFavorite(product);
    setIsFavorite(!isFavorite);
  };

  return (
    <div className={cn("card", className)}>
      <ButtonCornerCart
        onClick={onModalProduct}
        counterBasket={counterBasket}
        isActive={product.addedToCart}
      />
      <ButtonCornerHeart onClick={toggleFavorite} isActive={isFavorite} />

      <CardContent product={product} />
    </div>
  );
};

CardGeneral.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  onModalProduct: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CardGeneral;
