import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./CardElement.scss";

const CardElement = ({ className, children }) => {
  return <button className={cn("card__element", className)}>{children}</button>;
};

CardElement.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default CardElement;
