import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Cards.scss";

import CardElement from "./CardElements/CardElement";

const CardContent = ({ className, product }) => {
  const { vendorCode, name, price, colorCode, path } = product;

  return (
    <div className={cn("card__content", className)}>
      <header className={cn("card__header", className)}>{name}</header>
      <main className={cn("card__main", className)}>
        <img src={path} alt={name} className="card__main-img" />
      </main>
      <footer className={cn("card__footer", className)}>
        {/* <p>Price: ${price}</p> */}
        <p>Article: {vendorCode}</p>
      </footer>
      <CardElement>
        <div
          className="card__color"
          style={{ backgroundColor: colorCode }}
        ></div>
      </CardElement>
      <p className="card__price">${price}</p>
    </div>
  );
};

CardContent.propTypes = {
  className: PropTypes.string,
  product: PropTypes.shape({
    vendorCode: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    colorCode: PropTypes.string,
    path: PropTypes.string.isRequired,
  }).isRequired,
};

export default CardContent;
