import React from "react";
import PropTypes from "prop-types";

import ButtonCorner from "./ButtonCorner";
import Close from "../icons/close.svg?react";

const ButtonCornerClose = ({ onClick }) => {
  return (
    <ButtonCorner onClick={onClick}>
      <Close className="svg svg--close" />
    </ButtonCorner>
  );
};

ButtonCornerClose.propTypes = {
  onClick: PropTypes.func,
};

export default ButtonCornerClose;
