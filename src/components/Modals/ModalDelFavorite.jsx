import React from "react";
import PropTypes from "prop-types";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalHeader,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonClassic, { ButtonCornerClose } from "../Buttons";

const ModalDelFavorite = (props) => {
  const { isOpen, onClose, removeFavorite, currentProduct } = props;
  const { name, path } = currentProduct;

  const handleRemoveFavorite = () => {
    removeFavorite(currentProduct);
    onClose();
  };

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={onClose} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal__header-img-box">
              <img src={path} alt={name} className="modal__header-img" />
            </div>
          </ModalHeader>

          <ModalMain currentProduct={currentProduct}>
            Delete <span>{name}</span> from favorites
          </ModalMain>

          <ModalFooter>
            <div className="modal__footer-wrapper">
              <ButtonClassic className="focus" onClick={onClose}>
                NO, CANCEL
              </ButtonClassic>
              <ButtonClassic onClick={handleRemoveFavorite}>
                YES, DELETE
              </ButtonClassic>
            </div>
          </ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalDelFavorite.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  removeFavorite: PropTypes.func,
  currentProduct: PropTypes.object,
};

export default ModalDelFavorite;
