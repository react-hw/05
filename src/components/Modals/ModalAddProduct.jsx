import React from "react";
import PropTypes from "prop-types";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalHeader,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonClassic, { ButtonCornerClose } from "../Buttons";

const ModalAddProduct = (props) => {
  const { isOpen, onClose, currentProduct, handleModalProduct, onAddToCart } =
    props;
  const { name, path } = currentProduct;

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={handleModalProduct} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal__header-img-box">
              <img src={path} alt={name} className="modal__header-img" />
            </div>
          </ModalHeader>

          <ModalMain currentProduct={currentProduct}>
            Add <span>{name}</span> to cart
          </ModalMain>

          <ModalFooter>
            <ButtonClassic
              onClick={() => {
                handleModalProduct();
                onAddToCart();
              }}
            >
              ADD TO CART
            </ButtonClassic>
          </ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalAddProduct.propTypes = {
  isOpen: PropTypes.bool,
  currentProduct: PropTypes.object,
  onClose: PropTypes.func,
  handleModalProduct: PropTypes.func,
  onAddToCart: PropTypes.func,
};

export default ModalAddProduct;
