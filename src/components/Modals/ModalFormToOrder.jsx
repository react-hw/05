import React, { useRef } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";

import ModalWrapper, { ModalBox, ModalFooter } from "./components";

import ButtonClassic, { ButtonCornerClose } from "../Buttons";
import FormToCard from "../Form/FormToCard/FormToCard";

import "./ModalFormToOrder.scss";

import { setProducts, setToCart, setFavorite } from "../../store/rootSlice";
import {
  selectorProducts,
  selectorFavorites,
  selectorCarts,
} from "../../store/selectors";

const ModalFormToOrder = (props) => {
  const { isOpen, onClose } = props;

  const dispatch = useDispatch();
  const products = useSelector(selectorProducts);
  const favorites = useSelector(selectorFavorites);
  const carts = useSelector(selectorCarts);

  const formRef = useRef(null);

  const clearStoreForCarts = () => {
    const updateProducts = products.map((product) => ({
      ...product,
      addedToCart: false,
      basketCounter: 0,
    }));

    dispatch(setProducts(updateProducts));
    dispatch(setToCart([]));

    localStorage.setItem("products", JSON.stringify(updateProducts));
    localStorage.removeItem("carts");

    const updateFavorites = favorites.map((favorite) => ({
      ...favorite,
      addedToCart: false,
      basketCounter: 0,
    }));

    dispatch(setFavorite(updateFavorites));
    localStorage.setItem("favorites", JSON.stringify(updateFavorites));
  };

  const addInformationFromCart = () => {
    console.log(`You have ordered so products:`);

    carts.map((cart) => {
      const { name, basketCounter, colorText, price } = cart;
      console.log(`
    The Toy's name is "${name}":
          • Color:      ${colorText};
          • Amount:     ${basketCounter} pcs;
          • Price 
            (for one):  $${price};
          • To pay:     $${price * basketCounter};
          `);
    });
  };

  // const onCheckout = () => {
  //   if (formRef.current) {
  //     formRef.current.submitForm();
  //   }
  //   addInformationFromCart();
  //   clearStoreForCarts();
  //   onClose();
  // };

  const onCheckout = async () => {
    if (formRef.current) {
      const errors = await formRef.current.validateForm();
      if (Object.keys(errors).length === 0) {
        await formRef.current.submitForm();
        addInformationFromCart();
        clearStoreForCarts();
        onClose();
      }
    }
  };

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox className={"modal--order"}>
        <ButtonCornerClose onClick={onClose} />
        <div className="modal-container--order">
          <FormToCard ref={formRef} />
          <ModalFooter>
            <ButtonClassic className={"btn-checkout"} onClick={onCheckout}>
              Checkout
            </ButtonClassic>
          </ModalFooter>
        </div>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalFormToOrder.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

export default ModalFormToOrder;
